wp.blocks.registerBlockType('brad/border-box', {
   'title' : 'My Cool Border Box',
   'icon' : 'smiley',
   'category' : 'common',
   'attributes' : {
      'content' : { 'type': 'string' },
      'color' : { 'type': 'string' }
   },

   // for admin dashboard
   'edit' : function(props) {
      function updateContent(event) {
         props.setAttributes({
            content: event.target.value
         });
      }

      function updateColor(value) {
         props.setAttributes({
            color: value.hex
         });
      }

      // html in react.js way
      // React is globally available currently in WP
      // abstraction layer for React.createElement() = wp.element
      // check out create-gluten-block package

      /*
      <div>
        <h3>Your Cool Border Box</h3>
        <input type="text" value={props.attributes.content} onChange={updateContent} />
        <wp.components.ColorPicker color={props.attributes.color} onChangeComplete={updateColor} />
      </div>
      */

      return React.createElement(
         "div",
         null,
         React.createElement(
            "h3",
            {
               style: {
                  border: "5px solid ".concat(props.attributes.color)
               }
            },
            "Preview: ".concat(props.attributes.content)
         ),
         React.createElement("input", {
            type: "text",
            value: props.attributes.content,
            onChange: updateContent
         }),
         React.createElement(wp.components.ColorPicker, {
            color: props.attributes.color,
            onChangeComplete: updateColor
         })
      );
   },

   // for frontend
   'save' : function(props) {
      /*
      <h3 style={{border: `5px solid ${props.attributes.color}` }}>{props.attributes.content}</h3>
      */
      return React.createElement(
        "h3",
        {
          style: {
            border: "5px solid ".concat(props.attributes.color)
          }
        },
        props.attributes.content
      );
   }
});