<?php

/**
update_option('siteurl', 'http://fictional-university.test');
update_option('home', 'http://fictional-university.test');
*/

function universityQueryVars($vars) {
  $vars[] = 'skyColor';
  $vars[] = 'grassColor';
  return $vars;
}

add_filter('query_vars', 'universityQueryVars');

require get_theme_file_path('/inc/search-route.php');
require get_theme_file_path('/inc/like-route.php');

function university_custom_rest() {
  // 1st = post type
  // 2nd = property you want to add
  // 3rd = get_call_back return value will be assigned to 2nd
  register_rest_field('post', 'authorName', array(
    'get_callback' => function () {
      return get_the_author();
    }
  ));

  register_rest_field('note', 'userNoteCount', array(
    'get_callback' => function () {
      return count_user_posts(get_current_user_id(), 'note');
    }
  ));
}

// 1st argument = hook
// 2nd argument = function you want to add
add_action('rest_api_init', 'university_custom_rest');

function pageBanner($args=NULL) {
  if (!$args['title']) {
    $args['title'] = get_the_title();
  }

  if (!$args['subtitle']) {
    $args['subtitle'] = get_field('page_banner_subtitle');
  }

  if (!$args['photo']) {
    if (get_field('page_banner_background_image')) {
      $args['photo'] = get_field('page_banner_background_image')['sizes']['pageBanner'];
    }
    else {
      $args['photo'] = get_theme_file_uri('/images/ocean.jpg');
    }
  }
?>
  
  <div class="page-banner">
    <div class="page-banner__bg-image" style="background-image: url(<?php
      echo $args['photo'];
    ?>);"></div>
    <div class="page-banner__content container container--narrow">
      <h1 class="page-banner__title"><?php echo $args['title']; ?></h1>
      <div class="page-banner__intro">
        <p><?php echo $args['subtitle']; ?></p>
      </div>
    </div>  
  </div>

<?php
}

function university_files() {
  // NULL argument = not dependent on any files
  // 1.0 = version
  // true = load after html instead of head section
  wp_enqueue_script('googleMap', '//maps.googleapis.com/maps/api/js?key=AIzaSyCs5ranzSAn1_uvaNI2en14DOV9VtFnLDc', NULL, '1.0', true);
  wp_enqueue_script('main-university-js', get_theme_file_uri('/js/scripts-bundled.js'), NULL, '1.0', true);

  wp_enqueue_style('custom-google-fonts', '//fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i|Roboto:100,300,400,400i,700,700i');
  wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
  wp_enqueue_style('university_main_styles', get_stylesheet_uri());

  wp_localize_script('main-university-js', 'universityData', array(
    'root_url' => get_site_url(),
    'nonce' => wp_create_nonce('wp_rest')
  ));
}

// front-end
add_action('wp_enqueue_scripts', 'university_files');

function university_features() {
  add_theme_support('title-tag');
  add_theme_support('post-thumbnails');  // support featured image

  add_image_size('professorLandscape', 400, 260, true);
  add_image_size('professorPortrait', 480, 650, true);

  add_image_size('pageBanner', 1500, 350, true);
}

add_action('after_setup_theme', 'university_features');

function university_adjust_queries($query) {
  // is_admin() = if in admin dashboard
  // is_main_query() = if this is the default url based query

  if (!is_admin() AND is_post_type_archive('event') AND $query->is_main_query()) {

    // $query object is like a custom query new WP_Query()

    $query->set('meta_key', 'event_date');
    $query->set('orderby', 'meta_value_num');
    $query->set('order', 'ASC');

    $today = date('Ymd');
    $query->set('meta_query', array(
      array(
        'key' => 'event_date',
        'compare' => '>=',
        'value' => $today,
        'type' => 'numeric'
      )
    ));
  }

  if (!is_admin() AND is_post_type_archive('program') AND $query->is_main_query()) {

    // $query object is like a custom query new WP_Query()

    $query->set('orderby', 'title');
    $query->set('order', 'ASC');
    $query->set('posts_per_page', -1);  // -1 = all

  }

  if (!is_admin() AND is_post_type_archive('campus') AND $query->is_main_query()) {

    // $query object is like a custom query new WP_Query()

    $query->set('posts_per_page', -1);  // -1 = all

  }
}

add_action('pre_get_posts', 'university_adjust_queries');

function universityMapKey($api) {
  $api['key'] = 'AIzaSyCs5ranzSAn1_uvaNI2en14DOV9VtFnLDc';
  return $api;
}

add_filter('acf/fields/google_map/api', 'universityMapKey');

// Redirect subscriber accounts out of admin and onto homepage
function redirectSubsToFrontend() {
  $ourCurrentUser = wp_get_current_user();

  if (count($ourCurrentUser->roles) AND $ourCurrentUser->roles[0] == 'subscriber') {
    wp_redirect(site_url('/'));
    exit;
  }
}

add_action('admin_init', 'redirectSubsToFrontend');

function noSubsAdminBar() {
  $ourCurrentUser = wp_get_current_user();

  if (count($ourCurrentUser->roles) AND $ourCurrentUser->roles[0] == 'subscriber') {
    show_admin_bar(false);
  }
}

add_action('wp_loaded', 'noSubsAdminBar');

// Customize Login Screen
add_filter('login_headerurl', 'ourHeaderUrl');

function ourHeaderUrl() {
  return esc_url(site_url('/'));
}

// Login Screens
add_action('login_enqueue_scripts', 'ourLoginCSS');

function ourLoginCSS() {
  wp_enqueue_style('custom-google-fonts', '//fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i|Roboto:100,300,400,400i,700,700i');
  wp_enqueue_style('university_main_styles', get_stylesheet_uri());
}

add_filter('login_headertitle', 'ourLoginTitle');

function ourLoginTitle() {
  return get_bloginfo('name');
}

// Force note posts to be private
// This hook will run for all post types
// Filter runs when creating and editing a post
// 10 = priority if there are many functions hooked
// 2 = work with 2 parameters, default = 1
add_filter('wp_insert_post_data', 'makeNotePrivate', 10, 2);

function makeNotePrivate($data, $postArr) {
  if ($data['post_type'] == 'note') {
    // $postArr because there's no intuitive way to know it's create
    if (count_user_posts(get_current_user_id(), 'note') > 4 AND !$postArr['ID']) {
      // anything after this will not be executed
      die('You have reached your note limit.');
    }

    $data['post_content'] = sanitize_textarea_field($data['post_content']);
    $data['post_title'] = sanitize_text_field($data['post_title']);
  }

  if ($data['post_type'] == 'note' AND $data['post_status'] != 'trash') {
    $data['post_status'] = 'private';
  }
  
  return $data;
}