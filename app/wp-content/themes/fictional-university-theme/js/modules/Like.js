import $ from 'jquery';

class Like {
   constructor() {
      this.events();
   }

   events() {
      $('.like-box').on('click', this.ourClickDispatcher.bind(this));
   }

   // methods
   ourClickDispatcher(e) {
      var currentLightBox = $(e.target).closest('.like-box');

      // attr for on the fly
      if (currentLightBox.attr('data-exists') == 'yes') {
         this.deleteLike(currentLightBox);
      }
      else {
         this.createLike(currentLightBox);
      }
   }

   createLike(currentLightBox) {
      $.ajax({
         beforeSend: (xhr) => {
           xhr.setRequestHeader('X-WP-Nonce', universityData.nonce);
         },
         
         url: universityData.root_url + '/wp-json/university/v1/manageLike',
         type: 'POST',
         data: {
            'professorId' : currentLightBox.data('professor') 
         },

         success: (response) => {
            currentLightBox.attr('data-exists', 'yes');
            var likeCount = parseInt(currentLightBox.find('.like-count').html(), 10);
            likeCount++;

            currentLightBox.find('.like-count').html(likeCount);
            currentLightBox.attr('data-like', response);
            console.log(response);
         },
         error: (response) => {
            console.log(response);
         }
      });
   }

   deleteLike(currentLightBox) {
      $.ajax({
         beforeSend: (xhr) => {
           xhr.setRequestHeader('X-WP-Nonce', universityData.nonce);
         },

         url: universityData.root_url + '/wp-json/university/v1/manageLike',
         data: {
            'like' : currentLightBox.attr('data-like')
         },
         type: 'DELETE',

         success: (response) => {
            currentLightBox.attr('data-exists', 'no');
            var likeCount = parseInt(currentLightBox.find('.like-count').html(), 10);
            likeCount--;

            currentLightBox.find('.like-count').html(likeCount);
            currentLightBox.attr('data-like', '');

            console.log(response);
         },
         error: (response) => {
            console.log(response);
         }
      });
   }
}

export default Like;