<?php

add_action('rest_api_init', 'universityLikeRoutes');

function universityLikeRoutes() {
  register_rest_route('university/v1', 'manageLike', array(
    'methods' => 'POST',
    'callback' => 'createLike'
  ));

  register_rest_route('university/v1', 'manageLike', array(
    'methods' => 'DELETE',
    'callback' => 'deleteLike'
  ));
}

function createLike($data) {
  // you can use current_user_can('publish_note')
  // is_user_logged_in() relies on NONCE
  if (is_user_logged_in()) {
    $professor = sanitize_text_field($data['professorId']);

    $existQuery = new WP_Query(array(
      // get_current_user_id() returns 0 if logged out
      // assigning author = 0 is as good as it's not there
      'author' => get_current_user_id(),
      'post_type' => 'like',
      'meta_query' => array(
        array(
          'key' => 'liked_professor_id',
          'compare' => '=',
          'value' => $professor
        )
      )
    ));

    // make sure professor id is not made up
    if ($existQuery->found_posts == 0 AND get_post_type($professor) == 'professor') {
      // returns the id
      return wp_insert_post(array(
        'post_type' => 'like',
        'post_status' => 'publish',
        'post_title' => '2nd PHP Test',
        'meta_input' => array(
          'liked_professor_id' => $professor
        )
      ));
    }
    else {
      die('Invalid professor id');
    }
  }
  else {
    die('Only logged in users can create a like.');
  }
}

function deleteLike($data) {
  $likeId = sanitize_text_field($data['like']);

  if (get_current_user_id() == get_post_field('post_author', $likeId) AND get_post_type($likeId) == 'like') {
    // true = skips trashing
    wp_delete_post($likeId, true);
    return 'Congrats, like deleted!';
  }
  else {
    die("You don't have permission to delete that.");
  }
}