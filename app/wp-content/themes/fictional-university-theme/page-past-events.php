<?php
  get_header();
  pageBanner(array(
    'title' => 'Past Events',
    'subtitle' => 'Recap of our past events'
  ));
?>

  <div class="container container--narrow page-section">
  <?php
    $today = date('Ymd');
    $pastEvents = new WP_Query(array(
      'paged' => get_query_var('paged', 1),  // determines which page to query
      // 'posts_per_page' => 2, // default is 10
      'post_type' => 'event',  // default is 'post'
      'meta_key' => 'event_date',  // required if orderby = meta_value
      'orderby' => 'meta_value_num',  // 'meta_value' is for string
      'order' => 'ASC',
      'meta_query' => array(
        array(
          'key' => 'event_date',
          'compare' => '<',
          'value' => $today,
          'type' => 'numeric'
        )
      )
    ));

    while($pastEvents->have_posts()) {
      $pastEvents->the_post();
      get_template_part('template-parts/content-event');
    }

  echo paginate_links(array(
    'total' => $pastEvents->max_num_pages
  ));
  ?>
  </div>

<?php
  get_footer();
?>