<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
if (file_exists(__DIR__ . '/local.php')) {
   // Local database settings
   define( 'DB_NAME', 'dbname' );
   define( 'DB_USER', 'dbuser' );
   define( 'DB_PASSWORD', '123' );
   define( 'DB_HOST', 'localhost' );
}
else {
   // Live database settings
   define( 'DB_NAME', 'jonaraeo_universitydata' );
   define( 'DB_USER', 'jonaraeo_wp232' );
   define( 'DB_PASSWORD', 'W0rdpr3ss' );
   define( 'DB_HOST', 'localhost' );
}



/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '2c8iYQkA}j|r@LfCs>OgY{w0h}tJ>@}zNKVI$mY;U|$i+r<NvYenq@{y5*kRb~@O');
define('SECURE_AUTH_KEY',  '%pM kC@ZeH{_R9;6<~GoN1$ZdDWwSo{u3-qHa]iZBWwwGv-Ylm}#r,fnaP@QKfX}');
define('LOGGED_IN_KEY',    'izl}+)nK;Kj16b$TH<<dl|YaNBLYPjND|S)@c09TEd1Cz-;u=i5m5y!VcBSgD-,/');
define('NONCE_KEY',        '-VlL:|-Y%U7<<{LO4EN.$/:~7I2,XTe&K`XA v)%+LBF8j{-o;FV>>PG0;(Tv|U9');
define('AUTH_SALT',        'R3-$y>JR$*W|qcC)WOF$GPh>bB[Drs%PQ*MbG~>b.`U*y<>$t);K3+S`tl@PMt?i');
define('SECURE_AUTH_SALT', 'v+F|s+K-PK&,oSew ULX)OWEOObGUy1@qjLQZytb,K(^*>8F%J}oU)J-T6vTr0SY');
define('LOGGED_IN_SALT',   '79%WM/o%Pzk_8z;-=3H`|~3MaAzgFKZ8|TY|+4o1Eb2vL3.4BS2ZfV]qX+-kJP+}');
define('NONCE_SALT',       'vlq+zAaL!qy!H)X>oife7C6F.$]v@>fJ-:Tmq#$T3=O)hQPL]#G4 LTC0/$%W]-B');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
   define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
